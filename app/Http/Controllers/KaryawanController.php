<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $karyawan = Karyawan::all();

        return view('karyawan.index', compact('karyawan'));
    }
    public function create(Request $request)
    {
        \App\Models\Karyawan::create($request->all());
        return redirect('/')->with('sukses', 'Data Berhasil Dimasukkan!');
    }

    public function edit($id)
    {
        $karyawan = \App\Models\Karyawan::find($id);
        return view('karyawan/edit', ['karyawan' => $karyawan]);
    }

    public function update(Request $request,$id)
    {
        $karyawan = \App\Models\Karyawan::find($id);
        $karyawan->update($request->all());
        return redirect('/')->with('sukses', 'Data Berhasil Diubah!');
    }

    public function delete($id)
    {
        $karyawan = \App\Models\Karyawan::find($id);
        $karyawan->delete($karyawan);
        return redirect()->route('karyawan')->with('sukses', 'Data Berhasil Dihapus!');
    }
}
