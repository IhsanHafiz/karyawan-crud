@extends('layouts.master')

@section('content')     
        @if (session('sukses'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
               {{session('sukses')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6 px-0">
                <h1>Tabel Data Karyawan</h1>
            </div>
            <div class="col-md-6 d-flex align-items-center justify-content-end px-0">
                <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Tambahkan Karyawan
                </button>
            </div>
            <table class="table table-striped table-bordered border-primary">
                <tr class="table-primary">
                    <th>No.</th>
                    <th>Nama Lengkap</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Status</th>
                    <th>Jumlah Anak</th>
                    <th>Alamat</th>
                    <th>Action</th>
                </tr>
                @foreach ($karyawan as $k)
                <tr>
                    <td class="d-flex justify-content-center">{{$k->id}}</td>
                    <td>{{$k->nama_lengkap}}</td>
                    <td>{{$k->jenis_kelamin}}</td>
                    <td>{{$k->agama}}</td>
                    <td>{{$k->status}}</td>
                    <td>{{$k->jumlah_anak}}</td>
                    <td>{{$k->alamat}}</td>
                    <td class="d-flex justify-content-center"><a href="/karyawan/{{$k->id}}/edit" class="btn btn-warning btn-sm ml-2 mr-2">Edit</a>
                        <a href="/karyawan/{{$k->id}}/delete" class="btn btn-danger btn-sm mx-2" onclick="return confirm('Yakin mau Dihapus?')">Hapus</a>
                    </td>
                </tr>
                @endforeach
            </table>        
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambahkan Karyawan</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body">
                <form action="/karyawan/create" method="POST">
                    {{csrf_field()}}
                    <div class="mb-3">
                      <label for="InputNama" class="form-label">Nama Lengkap</label>
                      <input name="nama_lengkap" type="text" class="form-control" id="InputNama">
                    </div>
                    <div class="form-group mb-3">
                        <label for="Jenis Kelamin" class="form-label">Jenis Kelamin</label>
                        <select name="jenis_kelamin" class="form-select">
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="Agama" class="form-label">Agama</label>
                        <select name="agama" class="form-select">
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="Status" class="form-label">Status</label>
                        <select name="status" class="form-select">
                            <option value="Menikah">Menikah</option>
                            <option value="Belum Menikah">Belum Menikah</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="InputJumlahAnak" class="form-label">Jumlah Anak</label>
                        <input name="jumlah_anak" type="text" class="form-control" id="InputJumlahAnak">
                    </div>
                    <div class="mb-3">
                        <label for="Alamat" class="form-label">Alamat</label>
                        <textarea name="alamat" class="form-control" id="Alamat" rows="3"></textarea>
                     </div>
                     <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                  </form>
            </div>
          </div>
        </div>
      
  @endsection
   


