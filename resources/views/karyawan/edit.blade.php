@extends('layouts.master')

@section('content')
    

        <h1 class="d-flex justify-content-center">EDIT DATA KARYAWAN</h1>
        @if (session('sukses'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
               {{session('sukses')}}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <form action="/karyawan/{{$karyawan->id}}/update" method="POST">
                    {{csrf_field()}}
                    <div class="mb-3 mt-3">
                    <label for="InputNama" class="form-label">Nama Lengkap</label>
                    <input name="nama_lengkap" type="text" class="form-control" id="InputNama" value="{{$karyawan->nama_lengkap}}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="Jenis Kelamin" class="form-label">Jenis Kelamin</label>
                        <select name="jenis_kelamin" class="form-select">
                            <option value="L" @if($karyawan->jenis_kelamin == 'L') selected @endif>Laki-laki</option>
                            <option value="P" @if($karyawan->jenis_kelamin == 'P') selected @endif>Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="Agama" class="form-label">Agama</label>
                        <select name="agama" class="form-select">
                            <option value="Islam" @if($karyawan->agama == 'Islam') selected @endif>Islam</option>
                            <option value="Kristen" @if($karyawan->agama == 'Kristen') selected @endif>Kristen</option>
                            <option value="Katolik" @if($karyawan->agama == 'Katolik') selected @endif>Katolik</option>
                            <option value="Hindu" @if($karyawan->agama == 'Hindu') selected @endif>Hindu</option>
                            <option value="Buddha" @if($karyawan->agama == 'Buddha') selected @endif>Buddha</option>
                            <option value="Lainnya" @if($karyawan->agama == 'Lainnya') selected @endif>Lainnya</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="Status" class="form-label">Status</label>
                        <select name="status" class="form-select">
                            <option value="Menikah" @if($karyawan->status == 'Menikah') selected @endif>Menikah</option>
                            <option value="Belum Menikah"  @if($karyawan->status == 'Belum Menikah') selected @endif>Belum Menikah</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="InputJumlahAnak" class="form-label">Jumlah Anak</label>
                        <input name="jumlah_anak" type="text" class="form-control" id="InputJumlahAnak" value="{{$karyawan->jumlah_anak}}">
                    </div>
                    <div class="mb-3">
                        <label for="Alamat" class="form-label">Alamat</label>
                        <textarea name="alamat" class="form-control" id="Alamat" rows="3">{{$karyawan->alamat}}</textarea>
                    </div>
                    <div class="modal-footer  d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary btn-md btn- col-lg-12">Update</button>
                        <a href="{{route('karyawan')}}" class="btn btn-danger btn-md btn- col-lg-12">Kembali</a>
                    </div>
                </form>
            </div>
        </div>

    @endsection
